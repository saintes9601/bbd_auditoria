# -*- coding: utf-8 -*-
"""
Created on Mon Feb 24 13:25:04 2020

@author: Pablo Saldarriaga
"""
import os
import json
import pandas as pd
import datetime as dt
from sklearn.externals import joblib
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
#%%
current_dir = os.getcwd()

file_name = 'conf.json'
path = os.path.join(current_dir, f'{file_name}')
with open(path, 'r') as f:
    info_conf = json.load(f)
            
base_path = info_conf['base_path']
os.chdir(base_path)
#%%
#Define el archivo en el que se guararan los logs del codigo

import logging
from logging.handlers import RotatingFileHandler

file_name = 'training_recon'
logger = logging.getLogger()
dir_log = os.path.join(base_path, f'recon/data/logs/{file_name}.log')

handler = RotatingFileHandler(dir_log, maxBytes=2000000, backupCount=10)
logging.basicConfig(level=logging.DEBUG,
                    format="%(asctime)s - %(process)d - %(name)s - %(levelname)s - %(message)s",
                    handlers = [handler])
#%%
import recon.scripts.funciones as funciones
from recon.scripts.recon_class.clase_reconectador import Reconectador
#%%

def main(base_path,lags = 3, radio_km = 4, frecuencia = '10H'):
    
    d_ini = dt.datetime(2020,1,1)
    d_fin = dt.datetime(2020,2,17)
    
    version = 'v001'
    now_date = dt.datetime.now()
    desc = 'Modelo inicial para un reconectador'
    
    mod_reconectador = Reconectador(now_date, version, base_path, desc)
    
    cols_base = ['time']
    mod_reconectador.cols_base = cols_base
    
    models = {'linear': {'mod' : LogisticRegression(random_state = 42),
                        'par' : {'solver' : ('newton-cg', 'lbfgs'),
                                 'class_weight' : (None, 'balanced'),
                                 'penalty': ('l2','none'),
                                 'warm_start': [True, False]}
                        },
              'gradient': {'mod' : GradientBoostingClassifier(warm_start = True),
                        'par' : {'loss' : ('deviance', 'exponential'),
                                 'max_depth' : [2, 3, 4, 5, 6, 7],
                                 'criterion' : ('friedman_mse','mse','mae')}
                        },
              'rforest': {'mod' : RandomForestClassifier(),
                        'par' : {'n_estimators' : [10, 20, 50, 80, 100],
                                 'criterion' : ('gini', 'entropy'),
                                 'bootstrap': [True, False]}
                        }
                 }    
    
    mod_reconectador.models = models
    ### Organizacion de la informacion de las corrientes
    data = funciones.read_corrientes(base_path + '/'+'data/20200108 I U elemento 80902679 20190101 20191231.csv')
  
    info_clima = funciones.read_clima(base_path,d_ini, d_fin)
    var_clima = list(info_clima.drop(columns = ['COD_ELEMENTO','precipType','summary','time','uvIndex']).columns)
        
    incidencias = funciones.read_incidencias('recon/data/20200108 incidencias ini fin 80902679 20190101 20191231.csv')
        
    rec_info_path = os.path.join(base_path,'data', 'ubicacion_reconectadores.json')
    with open(rec_info_path, 'r') as f:
        rec_info = json.load(f)
    
    reconectadores = rec_info['Reconectadores']
    
    descargas = funciones.read_discharges(d_ini,d_fin)
    
    
    all_data, X, Y = mod_reconectador.transform(data, info_clima, descargas, incidencias, 
                  dummy_vars = ['icon'], var_clima =var_clima, reconectadores =reconectadores, lags = lags,
                  dist_km = radio_km, freq = frecuencia )
    
    
    X_test, Y_test, models, selected = mod_reconectador.train(X,Y,score = 'roc_auc',cv = 2,n_proc = 1,
                                                              desc_model_sav = '',prop_deseada_under = 0.05)
    

    #Realiza la prediccion de las fallas en un conjunto de datos de prueba
    model_sel = models[selected]['bestModel']
    preds_ff = mod_reconectador.predict(X_test,model_sel)
    preds_ff['Failure'] = Y_test
    
    #Realiza graficas de la curva ROC-AUC y diagramas de violin que permitan
    #analizar el comportamiento y deseméño del modelo
    funciones.graphs_evaluation(mod_reconectador.path_version, selected, preds_ff, save = True)
    
    #Umbrales
    bound = [0.4,0.5,0.6]
    
    #Obtencion de matrices de confusion para diferentes umbrales de la predicicon
    funciones.matrix_confusion(mod_reconectador.path_version, selected, preds_ff, bound[0],  save=True)
    funciones.matrix_confusion(mod_reconectador.path_version, selected, preds_ff, bound[1],  save=True)
    funciones.matrix_confusion(mod_reconectador.path_version, selected, preds_ff, bound[2],  save=True)
    
    
    # Guarda el modelo elegido y el objeto de clase NEUi como parte de un
    #pipeline    
    recon_pipe = Pipeline([('procesador', mod_reconectador),
                          ('modelo', models[selected]['bestModel'])])
    
    path_best_recon = os.path.join(mod_reconectador.path_version, f"{version}.sav")
    
    logger.info(f"Saving NEUI for version {version}")
    joblib.dump(recon_pipe, path_best_recon)
    
    logger.info("Finished program")    
    
    return None



if __name__ == '__main__':
    
    main(base_path)