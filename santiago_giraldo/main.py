# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 08:26:53 2020

@author: usuario
"""

import pandas as pd
import numpy as np
import logging

from codigo.calculos import (calcular_demanda_años,calcular_referencia_solar,
                             calcular_cuenta_potencia,calcular_cuenta_energia,
                             calcular_soc,organizar_cuenta_energia, 
                             organizar_cuenta_potencia,calcular_ahorro)

from codigo.exportar import exportar_para_power_bi,exportar_resultados

from codigo.parametros import (path_info,path_degradation,cliente,
                               soc_objetivo,min_soc,soc_valle_carga,
                               granularidad,año_fin,potencia_BESS,
                               energia_BESS,p_carga,precio_potencia_nocturno,
                               precio_potencia_valle,precio_potencia_punta,
                               precio_energia_nocturno,precio_energia_valle,
                               precio_energia_punta)

# Creación de logger
logger = logging.getLogger()
logger.setLevel(logging.INFO)
c_handler = logging.StreamHandler()
c_format=logging.Formatter("%(asctime)s:%(levelname)s:%(message)s")
c_handler.setFormatter(c_format)
logger.addHandler(c_handler)

logger.info('Cargando datos...')

info=pd.read_excel(path_info,'Hoja1')
degradation=pd.read_excel(path_degradation,'Hoja1')

# se organizan los dataframes para utilizar solo las columnas requeridas 
# se eliminan los faltantes
info.dropna(inplace=True)
degradation=degradation.iloc[:,:4]
info['Mes']=info['Fecha'].apply(lambda x:x.month)

#Asignación de variables

fecha=info['Fecha'].values
tipo_periodo=info['Tipo_Periodo'].values
bess_deg=degradation['BESS_Deg'].values
solar_deg=degradation['Solar_Deg'].values
eficiencia_bess=degradation['BESS_Ef'].values

#se crea numpy arrays de los valores de demanda base y k2_solares teniendo en cuenta la degradación
demanda_base=calcular_demanda_años(info['Demanda (KW)'],año_fin,np.ones((año_fin)))
kw_solares=calcular_demanda_años(info['Solar (KW)'],año_fin,solar_deg)

#Cálculo referencia caso solar
referencia_caso_solar=calcular_referencia_solar(info,potencia_BESS)

#Inicialización de algunas variables+
demanda_sol=np.zeros((len(demanda_base),año_fin))
p_BESS_solar=np.zeros((len(demanda_base),año_fin))

#Proceso

logger.info('Caculando demanda sol')

demanda_sol[:]=demanda_base - kw_solares
demanda_sol[demanda_sol<0]=0

#Calculo factura de potencia
logger.info(f'Calculando cuenta potencia inicial ')
max_mes_tipo=info[['Demanda (KW)','Mes','Tipo_Periodo']]

cuenta_potencia_base,max_mes_tipo_base= calcular_cuenta_potencia(max_mes_tipo,año_fin,precio_potencia_nocturno,
                                               precio_potencia_punta,precio_potencia_valle)

#Calculo de la energía y su cuenta  
logger.info(f'Calculando cuenta energia inicial')    
 
energia_base,cuenta_energia_base=calcular_cuenta_energia(demanda_base,tipo_periodo,granularidad,año_fin,
                                                         precio_energia_nocturno,precio_energia_punta,
                                                         precio_energia_valle)

logger.info(f'Calculando estado de la carga...')

(soc,dod,actuaciones_solbateria,
 violaciones_solbateria,
 fecha_violacion_solbateria)=calcular_soc(demanda_base,demanda_sol,año_fin,tipo_periodo,referencia_caso_solar,
                                          soc_objetivo,min_soc,soc_valle_carga,energia_BESS,potencia_BESS,bess_deg,
                                          p_BESS_solar,p_carga,eficiencia_bess,granularidad,fecha)

#Nuevo calcula factura potencia
logger.info(f'Calculando cuenta potencia final')

demanda_solbateria=pd.DataFrame(demanda_sol-p_BESS_solar)
max_mes_tipo=demanda_solbateria.copy()
demanda_solbateria=demanda_solbateria.values
max_mes_tipo[['Mes','Tipo_Periodo']]=info[['Mes','Tipo_Periodo']]

cuenta_potencia_solbateria,max_mes_tipo_solbateria= calcular_cuenta_potencia(max_mes_tipo,año_fin,precio_potencia_nocturno,
                                                     precio_potencia_punta,precio_potencia_valle)

# calcular factura de energia con nuevos valores
logger.info(f'Calculando cuenta energia final') 

energia_solbateria,cuenta_energia_solbateria=calcular_cuenta_energia(demanda_solbateria,tipo_periodo,granularidad,
                                                                     año_fin, precio_energia_nocturno,
                                                                     precio_energia_punta,precio_energia_valle)
    
#%%
logger.info('Calculando totales y ahorros')

cuenta_energia_base,cuenta_energia_mes_base,cuenta_energia_tipo_base=organizar_cuenta_energia(cuenta_energia_base,año_fin,info)

cuenta_energia_solbateria,cuenta_energia_mes_solbateria,cuenta_energia_tipo_solbateria=organizar_cuenta_energia(cuenta_energia_solbateria,
                                                                                                                 año_fin,info)

ahorro_cuenta_energia,ahorro_cuenta_energia_tipo,ahorro_cuenta_energia_mes= calcular_ahorro(cuenta_energia_base,cuenta_energia_solbateria
                                                                                            ,año_fin)

cuenta_potencia_base,cuenta_potencia_mes_base,cuenta_potencia_tipo_base=organizar_cuenta_potencia(cuenta_potencia_base,año_fin,
                                                                                                  max_mes_tipo_base)

cuenta_potencia_solbateria,cuenta_potencia_mes_solbateria,cuenta_potencia_tipo_solbateria=organizar_cuenta_potencia(cuenta_potencia_solbateria,
                                                                                                                    año_fin,
                                                                                                                    max_mes_tipo_solbateria)

ahorro_cuenta_potencia,ahorro_cuenta_potencia_tipo,ahorro_cuenta_potencia_mes= calcular_ahorro(cuenta_potencia_base,cuenta_potencia_solbateria
                                                                                            ,año_fin)

ahorro_total,ahorro_total_tipo,ahorro_total_mes= calcular_ahorro(ahorro_cuenta_energia,ahorro_cuenta_potencia,año_fin,1)

#Organización de output finales

logger.info('Exportando_datos...')

exportar_para_power_bi(info,demanda_solbateria,año_fin,soc,dod,p_BESS_solar,
                       cuenta_energia_base,cuenta_energia_solbateria,
                       cuenta_potencia_base,cuenta_potencia_solbateria,
                       ahorro_cuenta_energia,ahorro_cuenta_potencia,cliente)

exportar_resultados(actuaciones_solbateria,violaciones_solbateria,fecha_violacion_solbateria,ahorro_total_tipo,ahorro_cuenta_energia_tipo,
                    ahorro_cuenta_potencia_tipo,cuenta_energia_tipo_solbateria,cuenta_energia_tipo_base,cuenta_potencia_tipo_solbateria,
                    cuenta_potencia_tipo_base,soc,año_fin,cliente)

logger.info('Fin del programa')

    
    
    